//
//  SettingsViewController.swift
//  WeatherApp
//
//  Created by Matt Lee on 29/08/2019.
//  Copyright © 2019 AND Digital. All rights reserved.
//

import UIKit
import CoreLocation

class SettingsViewController: UIViewController {
    
    @IBOutlet var locationTextField: UITextField!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    let geocoder: CLGeocoder = CLGeocoder()

    @IBAction func saveButtonTapped() {
        
        guard let address = locationTextField.text else {
            return
        }
        
        spinner.startAnimating()
        
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            
            if let coordinates = placemarks?.first?.location?.coordinate {
                print(coordinates)
            }
            
            self.spinner.stopAnimating()
        }
        
    }

}
