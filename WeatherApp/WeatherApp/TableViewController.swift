//
//  TableViewController.swift
//  WeatherApp
//
//  Created by Matt Lee on 29/08/2019.
//  Copyright © 2019 AND Digital. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    let forecast: Forecast = Forecast(daily: DailyForecast(data: [
            ForecastEntry(time: 1567033200,
                          summary: "Mostly cloudy throughout the day.",
                          icon: "rain",
                          precipProbability: 0.42,
                          temperatureHigh: 17.65),
            ForecastEntry(time: 1567119600,
                          summary: "Overcast throughout the day.",
                          icon: "cloudy",
                          precipProbability: 0.09,
                          temperatureHigh: 18.94),
            ForecastEntry(time: 1567206000,
                          summary: "Partly cloudy throughout the day.",
                          icon: "partly-cloudy-day",
                          precipProbability: 0.06,
                          temperatureHigh: 18.39),
            ForecastEntry(time: 1567292400,
                          summary: "Possible drizzle overnight.",
                          icon: "partly-cloudy-day",
                          precipProbability: 0.3,
                          temperatureHigh: 15.47),
            ForecastEntry(time: 1567378800,
                          summary: "Partly cloudy throughout the day.",
                          icon: "rain",
                          precipProbability: 0.39,
                          temperatureHigh: 16.42),
        ]))

    // MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    // MARK: - UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.daily.data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherTableViewCell
        cell.configure(forecastEntry: forecast.daily.data[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Location: ???"
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }

}
