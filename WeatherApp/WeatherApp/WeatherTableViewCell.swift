//
//  WeatherTableViewCell.swift
//  WeatherApp
//
//  Created by Matt Lee on 29/08/2019.
//  Copyright © 2019 AND Digital. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet var precipChanceLabel: UILabel!
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var thumbnailImageView: UIImageView!

    func configure(forecastEntry: ForecastEntry) {
        
        configureTimeLabel(timestamp: forecastEntry.time)
        
        summaryLabel.text = forecastEntry.summary
        
        precipChanceLabel.text = "\(lround(forecastEntry.precipProbability*100))% chance of rain"
        
        temperatureLabel.text = "\(lround(forecastEntry.temperatureHigh))\u{00B0}C"
        
        thumbnailImageView.image = UIImage(named: forecastEntry.icon)
    }
    
    private func configureTimeLabel(timestamp: Int) {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "EEE, d MMM"
        
        timeLabel.text = "\(dateFormatter.string(from: date))"
    }

}
