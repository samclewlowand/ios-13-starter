//
//  Forecast.swift
//  WeatherApp
//
//  Created by Matt Lee on 29/08/2019.
//  Copyright © 2019 AND Digital. All rights reserved.
//

import Foundation

struct Forecast: Decodable {
    let daily: DailyForecast
}

struct DailyForecast: Decodable {
    let data: [ForecastEntry]
}

struct ForecastEntry: Decodable {
    let time: Int
    let summary: String
    let icon: String
    let precipProbability: Double
    let temperatureHigh: Double
}
